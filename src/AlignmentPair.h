/* Copyright 2012 Tobias Marschall
 * 
 * This file is part of CLEVER.
 * 
 * CLEVER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLEVER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLEVER.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ALIGNMENTPAIR_H_
#define ALIGNMENTPAIR_H_

#include <string>

#include "ReadGroups.h"

/** Class that represents alignments of a read pair. */
class AlignmentPair {
private:
	std::string name;
	unsigned int pair_nr;
	int read_group;
	int phred_sum1;
	std::string chrom1;
	unsigned int start1;
	unsigned int end1;
	std::string strand1;
	int phred_sum2;
	std::string chrom2;
	unsigned int start2;
	unsigned int end2;
	std::string strand2;
	double aln_pair_prob;
	double aln_pair_prob_ins_length;
public:
	/** Parse an alignment pair from a line. If no read_group information is available, 
	  * parameter read_groups can be 0. */
	AlignmentPair(const std::string& line, ReadGroups* read_groups = 0);

    unsigned int getPairNr() const {
        return pair_nr;
    }

    int getPhredSum1() const {
        return phred_sum1;
    }

    int getPhredSum2() const {
        return phred_sum2;
    }

    /** Returns probability that alignment pair is correct based on alignment scores alone. */
	double getProbability() const {
        return aln_pair_prob;
    }

	/** Returns probability that alignment pair is correct based on alignment scores and insert lenghts. */
    double getProbabilityInsertLength() const {
        return aln_pair_prob_ins_length;
    }

    std::string getChrom1() const {
        return chrom1;
    }

    std::string getChrom2() const {
        return chrom2;
    }

    unsigned int getEnd1() const {
        return end1;
    }

    unsigned int getEnd2() const {
        return end2;
    }

    std::string getName() const {
        return name;
    }

    unsigned int getStart1() const {
        return start1;
    }

    unsigned int getStart2() const {
        return start2;
    }

    std::string getStrand1() const {
        return strand1;
    }

    std::string getStrand2() const {
        return strand2;
    }

    int getReadGroup() const {
        return read_group;
    }

};

#endif /* ALIGNMENTPAIR_H_ */
