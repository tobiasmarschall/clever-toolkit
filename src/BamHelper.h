/* Copyright 2012 Tobias Marschall
 *
 * This file is part of CLEVER.
 *
 * CLEVER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLEVER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLEVER.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef BAMHELPER_H
#define BAMHELPER_H

#include <ostream>
#include <vector>
#include <memory>

#include <boost/unordered_set.hpp>

#include <bamtools/api/BamAux.h>
#include <bamtools/api/BamAlignment.h>
#include <bamtools/api/BamWriter.h>
#include <bamtools/api/BamReader.h>

#include "IndelLengthDistribution.h"
#include "PositionSet.h"
#include "VariationIndex.h"
#include "ShortDnaSequence.h"

/** Collection of (static) helper functions to handle BAM files, especially
 *  BAM files created by laser-core with YA tags for alternative cigar
 *  strings.
 */
class BamHelper {
public:
	typedef enum { NONE, PROPER_PAIR, DISTANT_PAIR, BOTH_MAPPED_NOT_PAIRED, READ1_UNMAPPED, READ2_UNMAPPED, BOTH_UNMAPPED } pairing_status_t;

	/** A pairing of two alignments to form an alignment pair. */
	typedef struct pairing_information_t {
		pairing_status_t status;
		// index of alignment of first/second read end
		short aln_idx1;
		short aln_idx2;
		// index of cigar string (aka "subalignment") of first/second alignment 
		short cigar_idx1;
		short cigar_idx2;
		std::vector<double> distribution1;
		std::vector<double> distribution2;
		double probability;
		pairing_information_t() : status(NONE), aln_idx1(-1), aln_idx2(-1), cigar_idx1(-1), cigar_idx2(-1), probability(-1.0) {}
		pairing_information_t(short aln_idx1, short aln_idx2) : status(NONE), aln_idx1(aln_idx1), aln_idx2(aln_idx2), cigar_idx1(0), cigar_idx2(0), probability(-1.0) {}
// 		pairing_information_t(short aln_idx1, short aln_idx2, short cigar_idx1, short cigar_idx2) : pairing_status(NONE), aln_idx1(aln_idx1), aln_idx2(aln_idx2), cigar_idx1(cigar_idx1), cigar_idx2(cigar_idx2), probability(-1.0) {}
		pairing_information_t(pairing_status_t status, short aln_idx1, short aln_idx2, short cigar_idx1, short cigar_idx2, double probability) : status(status), aln_idx1(aln_idx1), aln_idx2(aln_idx2), cigar_idx1(cigar_idx1), cigar_idx2(cigar_idx2), probability(probability) {}
	} pairing_information_t;

	/** Details on the alignment such as cigar string and phred scores. */
	typedef struct subalignment_t {
		std::vector<BamTools::CigarOp> cigar;
		// phred score of this alignment, including indel costs
		int phred_score;
		// phred score only counting mismatches, but no indels
		int mismatch_phred_score;
		// phred score incurred by soft clips
		int softclip_phred_score;
		// phred-scaled extra costs given by the aligner in an YE tag (i.e. for interchromosomal splits)
		int extra_phred_costs;
		subalignment_t() : cigar(), phred_score(-1), mismatch_phred_score(-1), softclip_phred_score(-1), extra_phred_costs(-1) {}
		subalignment_t(const std::vector<BamTools::CigarOp>& cigar, int phred_score, int mismatch_phred_score, int softclip_phred_score, int extra_phred_costs) : cigar(cigar.begin(), cigar.end()), phred_score(phred_score), mismatch_phred_score(mismatch_phred_score), softclip_phred_score(softclip_phred_score), extra_phred_costs(extra_phred_costs) {}
		int indel_count() const {
			int result = 0;
			for (size_t i=0; i<cigar.size(); ++i){
				if ((cigar[i].Type == 'I') || (cigar[i].Type == 'D')) result += 1;
			}
			return result;
		}
	} subalignment_t;

	/** Data type representing a set of alignments for a paired end read. */
	typedef struct read_t {
		// rerefences to the original BAM records
		const std::vector<BamTools::BamAlignment*>& alignments1;
		const std::vector<BamTools::BamAlignment*>& alignments2;
		// alternative alignments extracted from the BAM records
		std::vector<std::vector<subalignment_t> > subalignments1;
		std::vector<std::vector<subalignment_t> > subalignments2;
		read_t(const std::vector<BamTools::BamAlignment*>& alignments1, const std::vector<BamTools::BamAlignment*>& alignments2) : alignments1(alignments1), alignments2(alignments2) {}
	} read_t;

	typedef struct alignment_coordinate_t {
		int32_t ref_id;
		int32_t start;
		int32_t end;
		bool operator==(const alignment_coordinate_t& c) const { return (ref_id == c.ref_id) && (start == c.start) && (end == c.end); }
		alignment_coordinate_t() : ref_id(-1), start(0), end(0) {}
		alignment_coordinate_t(const BamTools::BamAlignment& aln) : ref_id(aln.RefID), start(aln.Position), end(aln.GetEndPosition()) {}
	} alignment_coordinates;

	typedef struct aln_pair_t {
		BamTools::BamAlignment* first;
		BamTools::BamAlignment* second;
		aln_pair_t() : first(0), second(0) {}
	} aln_pair_t;

private:
	typedef struct {
		bool operator()(const subalignment_t& s1, const subalignment_t& s2) {
			// 1. Criterion: PHRED score
			if (s1.phred_score != s2.phred_score) return s1.phred_score < s2.phred_score;
			// 2. Criterion number of indels
			int indel_count1 = s1.indel_count();
			int indel_count2 = s2.indel_count();
			if (indel_count1 != indel_count2) return indel_count1 < indel_count2;
			// 3. Prefer leftmost indels
			int i1 = 0;
			int i2 = 0;
			int pos1 = 0;
			int pos2 = 0;
			for (size_t j=0; j<indel_count1; ++j) {
				while ((s1.cigar[i1].Type != 'D') && (s1.cigar[i1].Type != 'I')) {
					if ((s1.cigar[i1].Type == 'D') || (s1.cigar[i1].Type == 'M') || (s1.cigar[i1].Type == 'X') || (s1.cigar[i1].Type == '=')) {
						pos1 += s1.cigar[i1].Length;
					}
					i1 += 1;
				}
				while ((s2.cigar[i2].Type != 'D') && (s2.cigar[i2].Type != 'I')) {
					if ((s2.cigar[i2].Type == 'D') || (s2.cigar[i2].Type == 'M') || (s2.cigar[i2].Type == 'X') || (s2.cigar[i2].Type == '=')) {
						pos2 += s2.cigar[i2].Length;
					}
					i2 += 1;
				}
				if (pos1 != pos2) return pos1 < pos2;
			}
			// 4. Prefer shorter CIGAR strings
			if (s1.cigar.size() != s2.cigar.size()) return s1.cigar.size() < s2.cigar.size();
			// 5. Last ressort tie breaker: lexicographical order
			for (size_t i=0; i<s1.cigar.size(); ++i) {
				if (s1.cigar[i].Length != s2.cigar[i].Length) return s1.cigar[i].Length < s2.cigar[i].Length;
				if (s1.cigar[i].Type != s2.cigar[i].Type) return s1.cigar[i].Type < s2.cigar[i].Type;
			}
			return false;
		}
	} subalignment_comparator_t;
	
	static void write_alignment_record(BamTools::BamWriter& bam_writer, const std::vector<BamTools::BamAlignment*>& alignments, const std::vector<std::vector<subalignment_t> >& subalignments, int aln_idx, int cigar_idx, const BamTools::BamAlignment* mate_aln, double probability, bool is_primary, bool retain_alternative_cigars, bool reduce_cigar,  bool readgroups_from_names, const std::string* readgroup, double pair_probability);
	static void compute_alignment_distribution(const std::vector<std::vector<subalignment_t> >& subalignments, std::vector<double>* target, int* best);
	static uint16_t probability_to_mapq(double p);
public:
	/** Parse a CIGAR string and append result to the given target vector. */
	static void parseCigar(const std::string& cigar, std::vector<BamTools::CigarOp>* target);

	/** Reads CIGAR string, AS, YM, and YA tags and writes all alternative alignments present
	 *  in the given alignment record to the target vector. The "main" alignment given by CIGAR 
	 *  string is written first.
	 */
	static void getSubalignments(const BamTools::BamAlignment& aln, std::vector<subalignment_t>* target);
	
	/** Extracts all subalignments and creates a record for a given read pair. */
	static std::unique_ptr<read_t> createReadRecord(const std::vector<BamTools::BamAlignment*>& alignments1, const std::vector<BamTools::BamAlignment*>& alignments2);

	/** Recompute PHRED score based on insertion/deletion length distributions, CIGAR string, and known PHRED scores of mismatches. */
	static void recalibratePhredScore(subalignment_t* subalignment, const BamTools::BamAlignment& aln, const IndelLengthDistribution& insertion_costs, const IndelLengthDistribution& deletion_costs, int softclip_open_costs = 35, int softclip_extend_costs = 3, PositionSet* snp_set = 0, int phred_offset = 33, VariationIndex* variation_set = 0);

	/** Recalibrates a list of subalignments and sorts them by phred score. */
	static void recalibratePhredScores(std::vector<subalignment_t>* subalignments, const BamTools::BamAlignment& aln, const IndelLengthDistribution& insertion_costs, const IndelLengthDistribution& deletion_costs, int softclip_open_costs = 35, int softclip_extend_costs = 3, PositionSet* snp_set = 0, int phred_offset = 33, VariationIndex* variation_set = 0);

	/** Analyzes options to pair up alignments for a read pair. If possible, pairing is done based on the posterior
	 * distribution over all possible pairs taking the internal segment size distribution, known SNPs, and known variants
	 * into account.
	 *  @param variation_set If given, known indels the lie in the internal segment will be taken into account
	 *                       when computing probability.
	 *  @param max_distance Maximum distance of reads in a \"regular\" pair. If distance is
	 *                      larger/interchromosomal, reads can still be paired, but only if 
	 *                      no pair with smaller distance is present. Value of -1 means "disabled".
	 */
	static std::unique_ptr<pairing_information_t> pairUpReads(const read_t& read, const HistogramBasedDistribution& internal_segment_size_dist, VariationIndex* variation_set = 0, int max_distance = -1, bool distant_pairs = false);
	
	/** Writes alignments for a read pair to a BAM file. 
	 *  @param read1_distribution Distribution over alignments of read 1 to be used, i.e., to base MAPQ values on.
	 *                            Can be computed either using pairUpReads (posterior over all pairs) or using 
	 *                            compute_alignment_distribution (distribution based on only one read).
	 *  @param read2_distribution See read1_distribution.
	 */
	static void writeAlignments(BamTools::BamWriter& bam_writer, const read_t& read, const pairing_information_t& pairing_info, bool retain_suboptimal = true, bool retain_alternative_cigars = true, bool reduce_cigar = false, bool readgroups_from_names = false, int strict_mapq_filter = 0, const std::string* readgroup = 0);

	/** Turns all 'X' and '=' events in a CIGAR string to 'M' events. */
	static void cigarReduceMismatches(std::vector<BamTools::CigarOp>* cigar);

	/** Compute NM tags (edit distance) based on CIGAR string. */
	static void addNMTag(BamTools::BamAlignment* aln);

	/** Expands the XA tag in a given alignment (if present) and appends corresponding records to "target". */
	static void expandXA(const BamTools::BamReader& bam_reader, const BamTools::BamAlignment& aln, std::vector<BamTools::BamAlignment*>* target, boost::unordered_set<BamHelper::alignment_coordinate_t>* coordinates_set = 0, long long* skipped_counter = 0);

	/** Returns variations, i.e. insertions and deletions, present in the given alignments. */
	static std::unique_ptr<std::vector<Variation> > variationsFromAlignment(const BamTools::RefVector& bam_ref_data, const BamTools::BamAlignment& aln);

	/** Reads all alignment pairs that lie in the given region and appends them to
	 *  the given vector "target". Ownership is transferred, i.e. the alignments 
	 *  must be deleted by the caller. Only full pairs, i.e. pair where both read ends are known, are reported.
	 *  Also, only primary alignments are returned. */
	static void readRegion(BamTools::BamReader& bam_reader, int chromosome_id, int start, int end, std::vector<aln_pair_t>* target);
	
	/** Determines whether the CIGAR string contains a given character. */
	static bool cigarContains(const BamTools::BamAlignment& aln, char character);

	/** If alignment is soft-clipped at the left end, returns the position just before the alignment
	 *  (i.e. the rightmost position on the reference not covered by the alignment. If read is not
	 *  soft-clipped, -1 is returned.
	 */
	static int leftSoftclipBreakpoint(const BamTools::BamAlignment& aln);

	/** If alignment is soft-clipped at the right end, returns the position just after the alignment
	 *  (i.e. the leftmost position on the reference not covered by the alignment. If read is not
	 *  soft-clipped, -1 is returned.
	 */
	static int rightSoftclipBreakpoint(const BamTools::BamAlignment& aln);
	
	/** Determines whether soft-clipping breakpoints (as returned by leftSoftclipBreakpoint and rightSoftclipBreakpoint) are
	 *  compatible with a given variation, i.e., whether the soft-clipping occured exactly at the variation breakpoints.
	 *  ATTENTION: only checks breakpoints, but not that chromosomes match.
	 */
	static bool softclipCompatibleWithVariant(int left_sc_breakpoint, int right_sc_breakpoint, const Variation& variation);
	
	/** Reads a BAM file in the regions of size +/- bam_window_size around the variants given as keys in map support_counts
	 *  and adds the found number of reads (as values) to the map support_counts.
	 *  @param add_new: If true, then also variants not yet present in "support_counts" are added.
	 *  @param mapq_threshold: If >0, then only reads meeting this minimum MAPQ are considered.
	 */
	static void findSupportingReads(BamTools::BamReader& bam_reader, int bam_window_size, bool add_new, boost::unordered_map<Variation,int>* support_counts, int mapq_threshold = 0);

	friend std::size_t hash_value(const BamHelper::alignment_coordinate_t& c);
};

std::ostream& operator<<(std::ostream& os, const BamHelper::alignment_coordinate_t& c);
std::ostream& operator<<(std::ostream& os, const std::vector<BamTools::CigarOp>& cigar);

#endif // BAMHELPER_H
