/* Copyright 2013 Tobias Marschall
 * 
 * This file is part of CLEVER.
 * 
 * CLEVER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLEVER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLEVER.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef DEFAULTGAPCOSTDISTRIBUTIONS_H
#define DEFAULTGAPCOSTDISTRIBUTIONS_H

#include "IndelLengthDistribution.h"

class DefaultGapCostDistributions {
public:
	static std::unique_ptr<IndelLengthDistribution> insertionCosts();
	static std::unique_ptr<IndelLengthDistribution> deletionCosts();
};

#endif // DEFAULTGAPCOSTDISTRIBUTIONS_H
