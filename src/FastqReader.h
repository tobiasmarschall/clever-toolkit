/* Copyright 2012 Tobias Marschall
 *
 * This file is part of CLEVER.
 *
 * CLEVER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLEVER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLEVER.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FASTQREADER_H_
#define FASTQREADER_H_

#include <boost/iostreams/filtering_stream.hpp>
#include <boost/unordered_map.hpp>

#include "ShortDnaSequence.h"

/** Reads FASTQ input from a stream. */
class FastqReader {
public:
	typedef struct fastq_record_t {
		std::string name;
		ShortDnaSequence sequence;
		fastq_record_t(const std::string& name, const std::string& seq, const std::string& qual) : name(name), sequence(seq, qual) {}
	} fastq_record_t;
private:
	boost::iostreams::filtering_istream& in;
	std::unique_ptr<fastq_record_t> next;
	bool remove_trailing_number;
	long long line_nr;
	void readNext();
public:
	/** If remove_trailing_number is true, then "/[0-9]" is removed from the end of the read (if present). */
	FastqReader(boost::iostreams::filtering_istream& in, bool remove_trailing_number = false);
	bool hasNext() const;
	std::unique_ptr<fastq_record_t> getNext();
};

#endif /* FASTQREADER_H_ */
