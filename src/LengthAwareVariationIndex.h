/* Copyright 2012 Tobias Marschall
 *
 * This file is part of CLEVER.
 *
 * CLEVER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLEVER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLEVER.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LENGTHAWAREVARIATIONINDEX_H_
#define LENGTHAWAREVARIATIONINDEX_H_

#include <vector>

#include "VariationIndex.h"

/** Index similar to VariationIndex, but optimized for queries for variations in
 *  a given length range. Internally, this index keeps multiple instances of
 *  VariationIndex for separate length ranges. */
class LengthAwareVariationIndex {
private:
	// Number of indexes in each direction (positive/negative)
	// the total number of indexes equals 2*INDEX_COUNT+1
	static const int INDEX_COUNT;
	const std::vector<Variation>& variations;
	// underlying variation indexes, each storing variations in a different length range
	std::vector<VariationIndex*> indexes;
	// data structure to map back the index of a variation from one sub index structure
	// to an index w.r.t. to the original list of variations
	std::vector<std::vector<std::size_t> > variation_index_map;
	// given a length, returns the index of the VariationIndex containing this length
	size_t getIndexByLength(int length);
public:
	LengthAwareVariationIndex(const std::vector<Variation>& variations);
	virtual ~LengthAwareVariationIndex();

	/** Returns the set of indices (w.r.t. to the vector given at construction time) of
	 *  variations that fulfill two conditions:
	 *  1) They are completely contained in the interval from start to end.
	 *     start/end are interpreted as positions between two characters, that is, a value n
	 *     refers to the position between character n and n-1. Returns a null pointer when there
	 *     are no variations in that region.
	 *  2) There length (as given by Variation::getLengthDifference lies in the specified
	 *     interval. */
	std::unique_ptr<std::vector<size_t> > containedIn(const std::string& chromosome, size_t start, size_t end, int min_length, int max_length);

};

#endif /* LENGTHAWAREVARIATIONINDEX_H_ */
