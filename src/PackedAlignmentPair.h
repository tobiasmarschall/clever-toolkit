/* Copyright 2012 Tobias Marschall
 * 
 * This file is part of CLEVER.
 * 
 * CLEVER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLEVER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLEVER.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PACKEDALIGNMENTPAIR_H_
#define PACKEDALIGNMENTPAIR_H_

#include "Types.h"
#include "AlignmentPair.h"

/** More space-saving representation of alignment pair that only
 *  contains the most important information. */
class PackedAlignmentPair {
private:
	std::string name;
	std::string chromosome;
	int read_group;
	unsigned int insert_start;
	unsigned int insert_length;
	unsigned int pair_nr;
	double aln_pair_prob_ins_length;
	alignment_id_t id;
public:
	PackedAlignmentPair(const AlignmentPair& ap);
	virtual ~PackedAlignmentPair();
	size_t intersectionLength(const PackedAlignmentPair& ap) const;
    unsigned int getInsertLength() const { return insert_length; }
    unsigned int getInsertStart() const { return insert_start; }
    unsigned int getInsertEnd() const { return insert_start + insert_length - 1; }
    std::string getName() const { return name; }
    std::string getChromosome() const { return chromosome; }
    int getReadGroup() const { return read_group; }
    double getWeight() const { return aln_pair_prob_ins_length; }
    alignment_id_t getID() const { return id; }
    void setID(alignment_id_t id) { this->id = id; }
	unsigned int getPairNr() const { return pair_nr; }
	/** Returns probability that alignment pair is correct based on alignment scores and insert lenghts. */
    double getProbabilityInsertLength() const { return aln_pair_prob_ins_length; }

};

#endif /* PACKEDALIGNMENTPAIR_H_ */
