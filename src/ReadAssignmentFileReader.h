/* Copyright 2012 Tobias Marschall
 * 
 * This file is part of CLEVER.
 * 
 * CLEVER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLEVER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLEVER.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef READASSIGNMENTFILEREADER_H_
#define READASSIGNMENTFILEREADER_H_

#include <memory>
#include <fstream>
#include <vector>

class ReadAssignmentFileReader {
private:
	typedef struct {
		std::string read_name;
		std::vector<std::pair<int,int> > assignments;
	} entry_t;
	std::ifstream ifs;
	std::unique_ptr<entry_t> current_entry;
	void parseEntry(const std::string& line);
public:
	ReadAssignmentFileReader(const std::string& filename);
	virtual ~ReadAssignmentFileReader();

	bool finished() const;
	void advance();
	const std::string& getReadName() const;
	/** Returns a vector of pairs (variation_id, alignment_pair_nr). */
	const std::vector<std::pair<int,int> >& getAssignments() const;
};

#endif /* READASSIGNMENTFILEREADER_H_ */
