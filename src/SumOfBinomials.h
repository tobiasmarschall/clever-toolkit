/* Copyright 2013 Tobias Marschall
 *
 * This file is part of CLEVER.
 *
 * CLEVER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLEVER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLEVER.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef SUMOFBINOMIALS_H
#define SUMOFBINOMIALS_H

#include <vector>

/** Class representing the distribution resulting from summing up several binomially distributed
 *  random variables.
 */
class SumOfBinomials {
private:
	typedef struct binomial_dist_t {
		size_t n;
		double p;
	} binomial_dist_t;
	// The resulting distribution
	std::vector<double>* dist;
	// Offset of the resulting distribution, ie dist[i-offset] gives P(X=i), where
	// X is the r.v. which is the sum of binomials
	int offset;
	// The total number of trials, i.e., the sum of n parameters of all binomials
	int n;
public:
	SumOfBinomials();
	virtual ~SumOfBinomials();

	/** Add binomially distributed random variable to the sum represented by this class. */
	void add(double p, int n);

	/** Returns the numbers of trials underlying this sum of binomal RVs. */
	int getTotal() { return n; }

	/** Returns the probability of a value of k. */
	double probability(int k);
};

#endif // SUMOFBINOMIALS_H
