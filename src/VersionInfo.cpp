/* Copyright 2013 Tobias Marschall
 * 
 * This file is part of CLEVER.
 * 
 * CLEVER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLEVER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLEVER.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sstream>
#include <cassert>

#include "VersionInfo.h"

using namespace std;

std::string VersionInfo::library_version = VERSION;

std::string VersionInfo::commandline(int argc, char* argv[]) {
	ostringstream oss;
	assert(argc > 0);
	oss << argv[0];
	for (int i=1; i<argc; ++i) {
		oss << ' ' << argv[i];
	}
	return oss.str();
}
