IF (EXISTS "${SRCDIR}/src/version.h")
	# If version header is provided in src dir, then don't generate anything
	EXECUTE_PROCESS(COMMAND ${CMAKE_COMMAND} -E copy ${SRCDIR}/src/version.h ${DSTDIR}/src/version.h)
ELSE()
	EXECUTE_PROCESS(COMMAND git describe --tags --dirty WORKING_DIRECTORY ${SRCDIR} RESULT_VARIABLE GIT_VERSION_AVAILABLE OUTPUT_VARIABLE GIT_VERSION ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE)
	IF (${GIT_VERSION_AVAILABLE} MATCHES 0)
		CONFIGURE_FILE("${SRCDIR}/src/version.h.template" "${DSTDIR}/src/version.h" @ONLY)
	ELSE()
		SET(GIT_VERSION "unknown")
		CONFIGURE_FILE("${SRCDIR}/src/version.h.template" "${DSTDIR}/src/version.h" @ONLY)
	ENDIF()
ENDIF()

