/* Copyright 2013 Tobias Marschall
 *
 * This file is part of CLEVER.
 *
 * CLEVER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLEVER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLEVER.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTERSPLITMAPPING_H_
#define INTERSPLITMAPPING_H_

#include <ostream>
#include <string>
#include <vector>
#include <bamtools/api/BamAlignment.h>

#include "../SplitAligner.h"
#include "../NamedDnaSequence.h"
#include "Mapping.h"

/** Split alignments resulting from one pair of anchors. */
class InterSplitMapping : public Mapping {
private:
	const std::vector<NamedDnaSequence*>* references;
	int anchor1_idx;
	bool anchor1_isleft;
	int anchor2_idx;
	bool anchor2_isleft;
	Read& parent;
	std::vector<SplitAligner::interchrom_split_alignment_t>* alignments;
	const Read::anchor_t& get_anchor1() const;
	const Read::anchor_t& get_anchor2() const;
public:
	InterSplitMapping(Read& parent);
	/** First anchor is the main anchor, i.e., alignment will be reported to be on that chromosome in the BAM output. */
	InterSplitMapping(const std::vector<NamedDnaSequence*>* references, int anchor1_idx, bool anchor1_isleft, int anchor2_idx, bool anchor2_isleft, Read& parent, std::vector<SplitAligner::interchrom_split_alignment_t>* alignments, int phred_score, int mismatch_phred_score);
	virtual ~InterSplitMapping();
	virtual BamTools::BamAlignment getBamAlignment(Read& parent, const std::string& name) const;
	virtual int getRefId() const;
	virtual int getStartPosition() const;
	virtual int getEndPosition() const;
	virtual bool isReverse() const;
	virtual int longestIndel() const;
	/** Returns a list containing only the "best" variation. Variation is canonified before being returned. */
	virtual std::unique_ptr<std::vector<Variation> > getPutativeVariations(const std::vector<NamedDnaSequence*>& reference_list, bool rightmost = false) const;
	virtual void print(std::ostream& os) const;
};

#endif /* INTERSPLITMAPPING_H_ */
