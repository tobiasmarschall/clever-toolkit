/* Copyright 2012 Tobias Marschall
 *
 * This file is part of CLEVER.
 *
 * CLEVER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLEVER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLEVER.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>

#include "../ShortDnaSequence.h"
#include "Read.h"
#include "WholeMapping.h"

using namespace std;

WholeMapping::WholeMapping(int ref_id, int start_position, int end_position, bool reverse, vector<BamTools::CigarOp> cigar, int phred_score, int mismatch_phred_score) : Mapping(phred_score,mismatch_phred_score), ref_id(ref_id), start_position(start_position), end_position(end_position), reverse(reverse), cigar(cigar) {
	assert(phred_score >= 0);
	probability = pow(10.0, -((double)phred_score)/10.0);
}

WholeMapping::~WholeMapping() {
}

BamTools::BamAlignment WholeMapping::getBamAlignment(Read& parent, const string& name) const {
	BamTools::BamAlignment a;
	a.Name = name;
	a.Position = start_position;
	a.RefID = ref_id;
	if (reverse) {
		a.SetIsReverseStrand(true);
		ShortDnaSequence rev = parent.getSequence().reverseComplement();
		a.QueryBases = rev.toString();
		a.Qualities = rev.qualityString();
	} else {
		a.SetIsReverseStrand(false);
		a.QueryBases = parent.getSequence().toString();
		a.Qualities = parent.getSequence().qualityString();
	}
	a.MapQuality = 255;
	a.CigarData = cigar;
	a.SetIsPaired(true);
	assert(phred_score >= 0);
	uint32_t as_tag = phred_score;
	if (!a.AddTag("AS", "I", as_tag)) assert(false);
	uint32_t ym_tag = mismatch_phred_score;
	if (!a.AddTag("YM", "I", ym_tag)) assert(false);
	return a;
}

int WholeMapping::getRefId() const {
	return ref_id;
}

int WholeMapping::getStartPosition() const {
	return start_position;
}

int WholeMapping::getEndPosition() const {
	return end_position;
}

bool WholeMapping::isReverse() const {
	return reverse;
}

int WholeMapping::longestIndel() const {
	int result = 0;
	vector<BamTools::CigarOp>::const_iterator it = cigar.begin();
	for (; it != cigar.end(); ++it) {
		if ((it->Type == 'I') || (it->Type == 'D')) {
			result = max(result, (int)it->Length);
		}
	}
	return result;
}

unique_ptr< std::vector< Variation > > WholeMapping::getPutativeVariations(const std::vector<NamedDnaSequence*>& reference_list, bool rightmost) const {
	return unique_ptr< std::vector< Variation > >();
}

void WholeMapping::print(std::ostream& os) const {
	os << getRefId() << ", " << getStartPosition() << " - " << getEndPosition() << ", PHRED: " << getPhredScore() << ", " << (isReverse()?"REVCOMP":"FORWARD") << ", WHOLE";
}
