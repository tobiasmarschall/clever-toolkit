#include <boost/test/unit_test.hpp>
#include "../src/CoverageMonitor.h"

using namespace std;


BOOST_AUTO_TEST_CASE( CoverageMonitorTest )
{
	CoverageMonitor cm;
	cm.addAlignment(PackedAlignmentPair(AlignmentPair("r1 1 -1 0 1  100  200 - 0 1  300  400 + 1.0 1.0")));
	cm.addAlignment(PackedAlignmentPair(AlignmentPair("r2 1 -1 0 1  120  230 - 0 1  330  430 + 1.0 1.0")));
	cm.addAlignment(PackedAlignmentPair(AlignmentPair("r3 1 -1 0 1  250  350 - 0 1  450  500 + 1.0 1.0")));
	cm.addAlignment(PackedAlignmentPair(AlignmentPair("r4 1 -1 0 1 8000 8100 - 0 1 18200 18300 + 1.0 1.0")));
	cm.addAlignment(PackedAlignmentPair(AlignmentPair("r5 1 -1 0 1 8050 8150 - 0 1 18250 18350 + 1.0 1.0")));

	BOOST_CHECK_EQUAL(cm.probeAlignment(PackedAlignmentPair(AlignmentPair("r6 1 -1 0 1 199 298 - 0 1 398 500 + 1.0 1.0"))), 3);
	BOOST_CHECK_EQUAL(cm.probeAlignment(PackedAlignmentPair(AlignmentPair("r7 1 -1 0 1 199 299 - 0 1 398 500 + 1.0 1.0"))), 2);
	BOOST_CHECK_EQUAL(cm.probeAlignment(PackedAlignmentPair(AlignmentPair("r8 1 -1 0 1 7199 7299 - 0 1 7398 7500 + 1.0 1.0"))), 1);
	BOOST_CHECK_EQUAL(cm.probeAlignment(PackedAlignmentPair(AlignmentPair("r9 1 -1 0 1 8050 8150 - 0 1 8250 8350 + 1.0 1.0"))), 3);
	BOOST_CHECK_EQUAL(cm.probeAlignment(PackedAlignmentPair(AlignmentPair("r10 1 -1 0 1 18099 18198 - 0 1 18299 18399 + 1.0 1.0"))), 3);
	BOOST_CHECK_EQUAL(cm.probeAlignment(PackedAlignmentPair(AlignmentPair("r11 1 -1 0 1 18099 18200 - 0 1 18299 18399 + 1.0 1.0"))), 2);
	BOOST_CHECK_EQUAL(cm.probeAlignment(PackedAlignmentPair(AlignmentPair("r12 1 -1 0 1 18150 18248 - 0 1 18349 19450 + 1.0 1.0"))), 2);
	BOOST_CHECK_EQUAL(cm.probeAlignment(PackedAlignmentPair(AlignmentPair("r13 1 -1 0 1 18150 18250 - 0 1 18349 19450 + 1.0 1.0"))), 1);

	BOOST_CHECK_EQUAL(cm.getCoverage(0), 0);
	BOOST_CHECK_EQUAL(cm.getCoverage(100), 0);
	BOOST_CHECK_EQUAL(cm.getCoverage(200), 0);
	BOOST_CHECK_EQUAL(cm.getCoverage(201), 1);
	BOOST_CHECK_EQUAL(cm.getCoverage(230), 1);
	BOOST_CHECK_EQUAL(cm.getCoverage(231), 2);
	BOOST_CHECK_EQUAL(cm.getCoverage(299), 2);
	BOOST_CHECK_EQUAL(cm.getCoverage(300), 1);
	BOOST_CHECK_EQUAL(cm.getCoverage(329), 1);
	BOOST_CHECK_EQUAL(cm.getCoverage(330), 0);
	BOOST_CHECK_EQUAL(cm.getCoverage(351), 1);
	BOOST_CHECK_EQUAL(cm.getCoverage(449), 1);
	BOOST_CHECK_EQUAL(cm.getCoverage(450), 0);

	BOOST_CHECK_EQUAL(cm.getCoverage(8100), 0);
	BOOST_CHECK_EQUAL(cm.getCoverage(8101), 1);
	BOOST_CHECK_EQUAL(cm.getCoverage(8151), 2);
	BOOST_CHECK_EQUAL(cm.getCoverage(8199), 2);
	BOOST_CHECK_EQUAL(cm.getCoverage(18200), 1);
	BOOST_CHECK_EQUAL(cm.getCoverage(18249), 1);
	BOOST_CHECK_EQUAL(cm.getCoverage(18250), 0);

	BOOST_CHECK_EQUAL(cm.probeAlignment(PackedAlignmentPair(AlignmentPair("r6 1 -1 0 1 199 298 - 0 1 398 500 + 1.0 1.0"))), 3);
	BOOST_CHECK_EQUAL(cm.probeAlignment(PackedAlignmentPair(AlignmentPair("r7 1 -1 0 1 199 299 - 0 1 398 500 + 1.0 1.0"))), 2);
	BOOST_CHECK_EQUAL(cm.probeAlignment(PackedAlignmentPair(AlignmentPair("r8 1 -1 0 1 7199 7299 - 0 1 7398 7500 + 1.0 1.0"))), 1);
	BOOST_CHECK_EQUAL(cm.probeAlignment(PackedAlignmentPair(AlignmentPair("r9 1 -1 0 1 8050 8150 - 0 1 8250 8350 + 1.0 1.0"))), 3);
	BOOST_CHECK_EQUAL(cm.probeAlignment(PackedAlignmentPair(AlignmentPair("r10 1 -1 0 1 18099 18198 - 0 1 18299 18399 + 1.0 1.0"))), 3);
	BOOST_CHECK_EQUAL(cm.probeAlignment(PackedAlignmentPair(AlignmentPair("r11 1 -1 0 1 18099 18200 - 0 1 18299 18399 + 1.0 1.0"))), 2);
	BOOST_CHECK_EQUAL(cm.probeAlignment(PackedAlignmentPair(AlignmentPair("r12 1 -1 0 1 18150 18248 - 0 1 18349 19450 + 1.0 1.0"))), 2);
	BOOST_CHECK_EQUAL(cm.probeAlignment(PackedAlignmentPair(AlignmentPair("r13 1 -1 0 1 18150 18250 - 0 1 18349 19450 + 1.0 1.0"))), 1);

	cm.pruneLeftOf(231);

	BOOST_CHECK_EQUAL(cm.getCoverage(231), 2);
	BOOST_CHECK_EQUAL(cm.getCoverage(299), 2);
	BOOST_CHECK_EQUAL(cm.getCoverage(300), 1);
	BOOST_CHECK_EQUAL(cm.getCoverage(329), 1);
	BOOST_CHECK_EQUAL(cm.getCoverage(330), 0);
	BOOST_CHECK_EQUAL(cm.getCoverage(351), 1);
	BOOST_CHECK_EQUAL(cm.getCoverage(449), 1);
	BOOST_CHECK_EQUAL(cm.getCoverage(450), 0);

	BOOST_CHECK_EQUAL(cm.getCoverage(8100), 0);
	BOOST_CHECK_EQUAL(cm.getCoverage(8101), 1);
	BOOST_CHECK_EQUAL(cm.getCoverage(8151), 2);
	BOOST_CHECK_EQUAL(cm.getCoverage(8199), 2);
	BOOST_CHECK_EQUAL(cm.getCoverage(18200), 1);
	BOOST_CHECK_EQUAL(cm.getCoverage(18249), 1);
	BOOST_CHECK_EQUAL(cm.getCoverage(18250), 0);

	BOOST_CHECK_EQUAL(cm.probeAlignment(PackedAlignmentPair(AlignmentPair("r6 1 -1 0 1 199 298 - 0 1 398 500 + 1.0 1.0"))), 3);
	BOOST_CHECK_EQUAL(cm.probeAlignment(PackedAlignmentPair(AlignmentPair("r7 1 -1 0 1 199 299 - 0 1 398 500 + 1.0 1.0"))), 2);
	BOOST_CHECK_EQUAL(cm.probeAlignment(PackedAlignmentPair(AlignmentPair("r8 1 -1 0 1 7199 7299 - 0 1 7398 7500 + 1.0 1.0"))), 1);
	BOOST_CHECK_EQUAL(cm.probeAlignment(PackedAlignmentPair(AlignmentPair("r9 1 -1 0 1 8050 8150 - 0 1 8250 8350 + 1.0 1.0"))), 3);
	BOOST_CHECK_EQUAL(cm.probeAlignment(PackedAlignmentPair(AlignmentPair("r10 1 -1 0 1 19050 19150 - 0 1 19250 19350 + 1.0 1.0"))), 1);
}
