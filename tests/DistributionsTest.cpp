#include <boost/test/unit_test.hpp>

#include <vector>

#include "../src/Distributions.h"

using namespace std;

BOOST_AUTO_TEST_CASE( DistributionsTest ) {
	vector<double> dist1;
	dist1.push_back(0.0);
	dist1.push_back(1e-250);
	dist1.push_back(0.2);
	dist1.push_back(0.8);
	dist1.push_back(1e-250);
	dist1.push_back(0.0);
	vector<double> dist2;
	dist2.push_back(0.0);
	dist2.push_back(0.0);
	dist2.push_back(1e-250);
	dist2.push_back(0.3);
	dist2.push_back(0.7);
	dist2.push_back(1e-250);
	dist2.push_back(0.0);
	dist2.push_back(0.0);
	int result_offset = 0;
	unique_ptr<vector<double> > result_dist = Distributions::convolve(dist1, dist2, 10, -3, &result_offset);
	BOOST_CHECK_EQUAL(result_offset, 11);
	BOOST_CHECK_EQUAL(result_dist->size(), 5);
	BOOST_CHECK_EQUAL(result_dist->at(0), 1e-250*0.3 + 1e-250*0.2);
	BOOST_CHECK_EQUAL(result_dist->at(1), 1e-250*0.7 + 0.2*0.3 + 0.8*1e-250);
	BOOST_CHECK_EQUAL(result_dist->at(2), 0.2*0.7 + 0.8*0.3);
	BOOST_CHECK_EQUAL(result_dist->at(3), 0.2*1e-250 + 0.8*0.7 + 1e-250*0.3);
	BOOST_CHECK_EQUAL(result_dist->at(4), 0.8*1e-250 + 1e-250*0.7);
}
